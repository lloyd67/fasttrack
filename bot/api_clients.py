import requests


class FasttrackClient:
    def __init__(self, api_key):
        self.api_key = api_key

    def data(self):
        data_dict = {}
        user_list = self.get_user_list()
        for uuid in user_list:
            user_detail = self.get_user_detail(uuid)
            if user_detail:
                data_dict[uuid] = {
                    'date_start': user_detail['date_start'],
                    'date_end': user_detail['date_end'],
                    'stars': user_detail['stars'],
                }
            else:
                data_dict[uuid] = {}
        return data_dict.items()

    def get_user_list(self):
        params = {
            'api_key': self.api_key,
            'cookie_dict__anextour_filter__isnull': 'False',
            'format': 'json'
        }
        r = requests.get('https://dashboard.fstrk.io/api/partners/chats/', params=params)
        if r.status_code != 200:
            return
        return [item['uuid'] for item in r.json()['results']]

    def get_user_detail(self, uuid):
        r = requests.get('https://dashboard.fstrk.io/api/partners/chats/%s/variables/?format=json' % uuid)
        if r.status_code != 200:
            return
        data = r.json()
        if data['anextour_filter']:
            user_detail = {
                'date_start': data['anextour_filter']['date']['beginDate'],
                'date_end': data['anextour_filter']['date']['endDate'],
                'stars': data['anextour_filter']['stars']
            }
        else:
            user_detail = {}
        return user_detail


class AnextourClient:
    def __init__(self, api_key, filters):
        self.api_key = api_key
        self.filters = filters

    def data(self):
        params = {
            'api': self.api_key,
        }
        # если фильтров у пользователя нет, выводим все предложения:
        if self.filters.get('date_start'):
            params['CHECKIN_BEG'] = '%d-%d-%d' % (
                self.filters['date_start']['year'],
                self.filters['date_start']['month'],
                self.filters['date_start']['day']
            )
        if self.filters.get('date_end'):
            params['CHECKIND_END'] = '%d-%d-%d' % (
                self.filters['date_end']['year'],
                self.filters['date_end']['month'],
                self.filters['date_end']['day']
            )
        if self.filters.get('stars'):
            params['STAR']: self.filters['stars']

        r = requests.get('http://searchtour.anextour.com:9999/lct/tour.php', params=params)
        if r.status_code != 200:
            return
        return r.json()
