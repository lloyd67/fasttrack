from django.core.management.base import BaseCommand
from django.conf import settings

from bot.api_clients import FasttrackClient, AnextourClient


class Command(BaseCommand):
    help = 'Parser for viber clients'

    def handle(self, *args, **options):
        ft = FasttrackClient(api_key=settings.BOT_KEY)
        for user, filters in ft.data():
            ac = AnextourClient(api_key=settings.TOUR_KEY, filters=filters)

            # результаты никуда не пишем и не разбираем, а тупо выводим в консоль
            print(user, ac.data())
